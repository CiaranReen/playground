GoPHP
=====

GoPHP is a PHP framework using the MVC architecture

Installation
============

In Linux

1) Download the zip and unzip to your directory

2) Open a terminal and cd to the GoPHP directory

3) Run './go' to create the recommended directory structure

